package com.simion.base;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.simion.util.Constantes;
import com.simion.util.Utils;

import java.util.ArrayList;

import okhttp3.internal.Util;

/**
 * Created by Simion on 07/03/2017.
 */
public class BaseDatos extends SQLiteOpenHelper {

    private static final String NOMBRE_BBDD = "farmacias_bbdd.db";
    private static final int DDBB_VERSION = 1;
    private static final String TABLA_FARMACIAS = "farmacias";

    public BaseDatos(Context context) {
        super(context, NOMBRE_BBDD, null, DDBB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_FARMACIAS + "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Constantes.NOMBRE + " TEXT, " + Constantes.DIRECCION + " TEXT, " + Constantes.TELEFONO +
                " INT, " + Constantes.HORARIO +" TEXT, " +Constantes.LATITUD + " DOUBLE, " +
                Constantes.LONGITUD + " DOUBLE, " + Constantes.IMAGES + " BLOB)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS eventos");
        onCreate(db);

    }

    public void nuevaFarmacia(Farmacia farmacia) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constantes.NOMBRE, farmacia.getNombre());
        values.put(Constantes.DIRECCION, farmacia.getDireccion());
        values.put(Constantes.TELEFONO, farmacia.getTelefono());
        values.put(Constantes.HORARIO, farmacia.getHorario());
        values.put(Constantes.LATITUD, farmacia.getLatitud());
        values.put(Constantes.LONGITUD, farmacia.getLongitud());
        values.put(Constantes.IMAGES, Utils.getBytes(farmacia.getFoto()));

        db.insertOrThrow(TABLA_FARMACIAS, null, values);
        db.close();
    }

    public void modificarFarmacia(Farmacia farmacia) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Constantes.NOMBRE, farmacia.getNombre());
        values.put(Constantes.DIRECCION, farmacia.getDireccion());
        values.put(Constantes.TELEFONO, farmacia.getTelefono());
        values.put(Constantes.HORARIO, farmacia.getHorario());
        values.put(Constantes.LATITUD, farmacia.getLatitud());
        values.put(Constantes.LONGITUD, farmacia.getLongitud());
        values.put(Constantes.IMAGES, Utils.getBytes(farmacia.getFoto()));

        String[] argumentos = new String[]{String.valueOf(farmacia.getId())};
        db.update(TABLA_FARMACIAS, values, "id = ?" , argumentos);
        db.close();
    }

    public void eliminarFarmacia(Farmacia farmacia){
        SQLiteDatabase db = getWritableDatabase();

        String[] argumentos = new String[]{String.valueOf(farmacia.getId())};
        db.delete(TABLA_FARMACIAS, "id = ?", argumentos);
        db.close();
    }

    public ArrayList<Farmacia> obtenerFarmacias(){


        final String[] SELECT = {Constantes._ID, Constantes.NOMBRE, Constantes.DIRECCION,
                Constantes.TELEFONO, Constantes.HORARIO, Constantes.LATITUD,
                Constantes.LONGITUD, Constantes.IMAGES};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLA_FARMACIAS, SELECT, null, null, null, null,
                Constantes.NOMBRE);

        ArrayList<Farmacia> listaFarmacias = new ArrayList<>();
        Farmacia farmacia = null;
        while (cursor.moveToNext()) {
            farmacia = new Farmacia();
            farmacia.setId(cursor.getInt(0));
            farmacia.setNombre(cursor.getString(1));
            farmacia.setDireccion(cursor.getString(2));
            farmacia.setTelefono(cursor.getInt(3));
            farmacia.setHorario(cursor.getString(4));
            farmacia.setLatitud(cursor.getDouble(5));
            farmacia.setLongitud(cursor.getDouble(6));
            farmacia.setFoto(Utils.getBitmap(cursor.getBlob(7)));


            listaFarmacias.add(farmacia);
        }
        db.close();

        return listaFarmacias;
    }
}

