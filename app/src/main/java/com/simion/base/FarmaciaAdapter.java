package com.simion.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.simion.farmaapp.R;

import java.util.ArrayList;

/**
 * Created by Simion on 07/03/2017.
 */
public class FarmaciaAdapter extends BaseAdapter {

    private final Context context;
    private final ArrayList<Farmacia> listaFarmacias;
    private final LayoutInflater inflater;

    public FarmaciaAdapter(Context context, ArrayList<Farmacia> listaFarmacias){
        this.context = context;
        this.listaFarmacias = listaFarmacias;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listaFarmacias.size();
    }

    @Override
    public Object getItem(int i) {
        return listaFarmacias.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.unafarmacia, null);

            holder = new ViewHolder();
            holder.foto = (ImageView) convertView.findViewById(R.id.ivImagen);
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.direccion = (TextView) convertView.findViewById(R.id.tvDireccion);
            holder.telefono = (TextView) convertView.findViewById(R.id.tvTelefono);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Farmacia farmacia = listaFarmacias.get(i);
        holder.foto.setImageBitmap(farmacia.getFoto());
        holder.nombre.setText(farmacia.getNombre());
        holder.direccion.setText(farmacia.getDireccion());
        holder.telefono.setText("Telefono: " + farmacia.getTelefono());

        return convertView;
    }

    static class ViewHolder {
        ImageView foto;
        TextView nombre;
        TextView direccion;
        TextView telefono;
    }
}
