package com.simion.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.simion.farmaapp.R;

import java.util.ArrayList;

/**
 * Created by Simion on 08/03/2017.
 */
public class OpinionAdapter extends BaseAdapter {
    private final Context context;
    private final ArrayList<Opinion> listaOpiniones;
    private final LayoutInflater inflater;

    public OpinionAdapter(Context context, ArrayList<Opinion> listaOpiniones) {
        this.context = context;
        this.listaOpiniones = listaOpiniones;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listaOpiniones.size();
    }

    @Override
    public Object getItem(int i) {
        return listaOpiniones.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.unaopnion, null);
            holder = new ViewHolder();
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombreOpinion);
            holder.comentario = (TextView) convertView.findViewById(R.id.tvComentarioOpinion);
            holder.puntuacion = (TextView) convertView.findViewById(R.id.tvPuntuacion);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Opinion opinion = listaOpiniones.get(i);
        System.out.printf("*adapter*2");
        holder.nombre.setText(opinion.getNombre());
        holder.comentario.setText(opinion.getComentario());
        holder.puntuacion.setText("Telefono: " + opinion.getPuntuacion());

        return convertView;
    }

    static class ViewHolder {
        TextView nombre;
        TextView comentario;
        TextView puntuacion;
    }
}