package com.simion.base;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.Toast;

import com.simion.util.Constantes;
import com.simion.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import uk.me.jstott.jcoord.LatLng;


/**
 * Created by Simion on 07/03/2017.
 */
public class TareaDescargarDatos extends AsyncTask<String, Void, Void>{

    private ArrayList<Farmacia> listaFarmacias;
    private FarmaciaAdapter adapter;

    public TareaDescargarDatos(ArrayList<Farmacia> listaFarmacias, FarmaciaAdapter adapter){
        this.listaFarmacias = listaFarmacias;
        this.adapter = adapter;
    }

    @Override
    protected Void doInBackground(String... urls) {

        String resultado;

        try {
            URL url = new URL(Constantes.URL_JSON);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            BufferedReader entrada = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String linea = null;

            while ((linea = entrada.readLine()) != null)
                sb.append(linea + "\n");
            entrada.close();
            resultado = sb.toString();

            JSONObject json = new JSONObject(resultado);
            JSONArray jsonArray = json.getJSONArray("features");
            Farmacia farmacia;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject properties = jsonArray.getJSONObject(i).getJSONObject("properties");
                String nombre = properties.getString("title");
                if (nombre.startsWith("Farmacia ")){
                    nombre = nombre.substring("Farmacia ".length()-1, nombre.length()-1);
                }
                String direccion = properties.getString("description");
                String [] partes = direccion.split("  Teléfono: ");
                direccion = partes[0];
                String horario = partes[1];
                int telefono = Integer.parseInt(partes[1].substring(0, 9));
                if (horario.length() > 9){
                    horario = partes[1].substring(9, horario.length());
                }
                url = new URL(properties.getString("icon"));
                Bitmap icon = null;
                try {
                    icon = BitmapFactory.decodeStream((InputStream)url.getContent());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                double este = jsonArray.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getDouble(0);
                double oeste = jsonArray.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getDouble(1);
                LatLng latLng = Utils.DeUMTSaLatLng(este, oeste);

                farmacia = new Farmacia();
                farmacia.setNombre(nombre);
                farmacia.setDireccion(direccion);
                farmacia.setTelefono(telefono);
                farmacia.setHorario(horario);
                farmacia.setFoto(icon);
                farmacia.setLatitud(latLng.getLat());
                farmacia.setLongitud(latLng.getLng());
                listaFarmacias.add(farmacia);
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (JSONException jsone) {
            jsone.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);

        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        adapter.notifyDataSetChanged();
    }
}
