package com.simion.base;

import android.graphics.Path;
import android.os.AsyncTask;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Simion on 08/03/2017.
 */
public class TareaOpinar extends AsyncTask<String, Void, Void> {

    private final OpinionAdapter adapter;
    private final ArrayList<Opinion> listaOpiniones;

    public TareaOpinar(ArrayList<Opinion> listaOpiniones, OpinionAdapter adapter){
        this.listaOpiniones = listaOpiniones;
        this.adapter = adapter;
    }
    @Override
    protected Void doInBackground(String... params) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        String sentencia = (String) params[0];
        Opinion[] opinionesArray = restTemplate.getForObject(sentencia, Opinion[].class);
        listaOpiniones.addAll(Arrays.asList(opinionesArray));
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);

        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        adapter.notifyDataSetChanged();
    }
}
