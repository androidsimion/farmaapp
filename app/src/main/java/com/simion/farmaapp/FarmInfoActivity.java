package com.simion.farmaapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.simion.util.Constantes;
import com.simion.util.Utils;

public class FarmInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farm_info);

        Intent intent = getIntent();
        String nombre = intent.getStringExtra(Constantes.NOMBRE);
        String direccion = intent.getStringExtra(Constantes.DIRECCION);
        String telefono = "Teléfono: " + intent.getIntExtra(Constantes.TELEFONO, 0);
        String horario = "Horario: " + intent.getStringExtra(Constantes.HORARIO);
        byte[] foto = intent.getByteArrayExtra(Constantes.IMAGES);

        TextView tvNombre = (TextView) findViewById(R.id.tvNombreInfo);
        TextView tvDireccion = (TextView) findViewById(R.id.tvDireccionInfo);
        TextView tvTelefono = (TextView) findViewById(R.id.tvTelefonoInfo);
        TextView tvHorario = (TextView) findViewById(R.id.tvHorarioInfo);
        ImageView ivFoto = (ImageView) findViewById(R.id.ivFotoInfo);

        tvNombre.setText(nombre);
        tvDireccion.setText(direccion);
        tvTelefono.setText(telefono);
        tvHorario.setText(horario);
        ivFoto.setImageBitmap(Utils.getBitmap(foto));
    }
}
