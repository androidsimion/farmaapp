package com.simion.farmaapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.simion.base.BaseDatos;
import com.simion.base.Farmacia;
import com.simion.base.FarmaciaAdapter;
import com.simion.base.TareaDescargarDatos;
import com.simion.util.Constantes;
import com.simion.util.Utils;

import java.util.ArrayList;

public class FarmaciasActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


    private ArrayList<Farmacia> listaFarmacias;
    private FarmaciaAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmacias);

        ListView lvListaFarmacias = (ListView) findViewById(R.id.lvListaFarmacias);
        listaFarmacias = new ArrayList<>();
        adapter = new FarmaciaAdapter(this, listaFarmacias);
        lvListaFarmacias.setAdapter(adapter);

        TareaDescargarDatos tdd = new TareaDescargarDatos(listaFarmacias, adapter);
        tdd.execute(Constantes.URL_JSON);


        lvListaFarmacias.setOnItemClickListener(this);
        registerForContextMenu(lvListaFarmacias);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Farmacia farmacia = listaFarmacias.get(position);
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra(Constantes.NOMBRE, farmacia.getNombre());
        intent.putExtra(Constantes.LATITUD, farmacia.getLatitud());
        intent.putExtra(Constantes.LONGITUD, farmacia.getLongitud());
        intent.putExtra(Constantes.TELEFONO, farmacia.getTelefono());
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_opiniones:
                intent = new Intent(this, OpinionesActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_nueva_farmacia:
                intent = new Intent(this, NuevaFarmaciaActivity.class);
                intent.putExtra(Constantes.ACCION, Constantes.NUEVA);
                startActivity(intent);
                return true;
            case R.id.menu_listar_farm_fav:
                intent = new Intent(this, FavFarmActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_acerca_de:
                intent = new Intent(this, AcercaDe.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_farm_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int posicion = menuInfo.position;
        Farmacia farmacia = listaFarmacias.get(posicion);
        switch (item.getItemId()){
            case R.id.action_ver_info:
                intent = new Intent(this, FarmInfoActivity.class);
                intent.putExtra(Constantes.NOMBRE, farmacia.getNombre());
                intent.putExtra(Constantes.DIRECCION, farmacia.getDireccion());
                intent.putExtra(Constantes.TELEFONO, farmacia.getTelefono());
                intent.putExtra(Constantes.HORARIO, farmacia.getHorario());
                intent.putExtra(Constantes.IMAGES, Utils.getBytes(farmacia.getFoto()));
                startActivity(intent);
                return true;
            case R.id.action_add_to_fav:
                BaseDatos db = new BaseDatos(this);
                db.nuevaFarmacia(farmacia);
                String mensaje = "La farmacia "+farmacia.getNombre()+" ha sido añadida a favoritos";
                Toast.makeText(this, mensaje, Toast.LENGTH_LONG);
                return true;
            case R.id.action_opinar:
                intent = new Intent(this, OpinarActivity.class);
                intent.putExtra(Constantes.NOMBRE, farmacia.getNombre());
                intent.putExtra(Constantes.DIRECCION, farmacia.getDireccion());
                intent.putExtra(Constantes.TELEFONO, farmacia.getTelefono());
                intent.putExtra(Constantes.IMAGES, Utils.getBytes(farmacia.getFoto()));
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }
}
