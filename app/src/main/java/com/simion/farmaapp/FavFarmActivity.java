package com.simion.farmaapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.common.io.Resources;
import com.simion.base.BaseDatos;
import com.simion.base.Farmacia;
import com.simion.base.FarmaciaAdapter;
import com.simion.util.Constantes;
import com.simion.util.Utils;

import java.util.ArrayList;

public class FavFarmActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ArrayList<Farmacia> listaFarmacias;
    FarmaciaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav_farm);

        listaFarmacias = new ArrayList<>();

        ListView lvFarmacias = (ListView) findViewById(R.id.lvFavFarm);
        adapter = new FarmaciaAdapter(this, listaFarmacias);
        lvFarmacias.setAdapter(adapter);

        BaseDatos bd = new BaseDatos(this);
        for (Farmacia farmacia:bd.obtenerFarmacias()){
            listaFarmacias.add(farmacia);
        }
        adapter.notifyDataSetChanged();


        lvFarmacias.setOnItemClickListener(this);
        registerForContextMenu(lvFarmacias);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Farmacia farmacia = listaFarmacias.get(position);
        if (farmacia.getLatitud() == 0 && farmacia.getLongitud() == 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            String mensaje = "Las coordenadas de esta farmacia son 0, 0. \n" +
                    "¿Esta seguro de que quiere mostrar en el mapa?";
            final Intent intent = new Intent(this, MapActivity.class);
            builder.setMessage(mensaje)
                    .setPositiveButton(R.string.lb_si,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    intent.putExtra(Constantes.NOMBRE, farmacia.getNombre());
                                    intent.putExtra(Constantes.LATITUD, farmacia.getLatitud());
                                    intent.putExtra(Constantes.LONGITUD, farmacia.getLongitud());
                                    intent.putExtra(Constantes.TELEFONO, farmacia.getTelefono());
                                    startActivity(intent);
                                }})
                    .setNegativeButton(R.string.lb_no,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }});
            builder.create().show();

        }
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        getMenuInflater().inflate(R.menu.add_farm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.menu_add_farm:
                intent = new Intent(this, NuevaFarmaciaActivity.class);
                intent.putExtra(Constantes.ACCION, Constantes.NUEVA);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.conetxt_fav_farm_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        AdapterView.AdapterContextMenuInfo menuInfo =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int posicion = menuInfo.position;
        final Farmacia farmacia = listaFarmacias.get(posicion);
        switch (item.getItemId()){
            case R.id.action_ver_info:
                intent = new Intent(this, FarmInfoActivity.class);
                intent.putExtra(Constantes.NOMBRE, farmacia.getNombre());
                intent.putExtra(Constantes.DIRECCION, farmacia.getDireccion());
                intent.putExtra(Constantes.TELEFONO, farmacia.getTelefono());
                intent.putExtra(Constantes.HORARIO, farmacia.getHorario());
                intent.putExtra(Constantes.IMAGES, Utils.getBytes(farmacia.getFoto()));
                startActivity(intent);
                return true;
            case R.id.action_modificar:
                intent = new Intent(this, NuevaFarmaciaActivity.class);
                intent.putExtra(Constantes._ID, farmacia.getId());
                intent.putExtra(Constantes.NOMBRE, farmacia.getNombre());
                intent.putExtra(Constantes.DIRECCION, farmacia.getDireccion());
                System.out.println("Modificrar -"+farmacia.getTelefono()+"- "+farmacia.getNombre());
                intent.putExtra(Constantes.TELEFONO, farmacia.getTelefono());
                intent.putExtra(Constantes.HORARIO, farmacia.getHorario());
                intent.putExtra(Constantes.IMAGES, Utils.getBytes(farmacia.getFoto()));
                intent.putExtra(Constantes.LATITUD, farmacia.getLatitud());
                intent.putExtra(Constantes.LONGITUD, farmacia.getLongitud());
                intent.putExtra(Constantes.ACCION, Constantes.MODIFICAR);
                startActivity(intent);
                return true;
            case R.id.action_eliminar:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                final BaseDatos db = new BaseDatos(this);
                builder.setMessage(R.string.lb_esta_seguro + farmacia.getNombre() + "?")
                        .setPositiveButton(R.string.lb_si,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        db.eliminarFarmacia(farmacia);
                                    }})
                        .setNegativeButton(R.string.lb_no,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }});
                builder.create().show();
                return true;
            default:
                return false;
        }
    }
}
