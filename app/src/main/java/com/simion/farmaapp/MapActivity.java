package com.simion.farmaapp;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationServices;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.simion.util.Constantes;
import com.simion.util.Utils;

public class MapActivity extends AppCompatActivity {

    MapView mapaView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MapboxAccountManager.start(this, "pk.eyJ1Ijoic2Fpa29jbyIsImEiOiJjaXkyYWE4YW4wMDEyMzN0MTN3ZGFyemM3In0.eALdTTh6xfcTgI3VEF78GQ");

        setContentView(R.layout.activity_map);
        mapaView = (MapView) findViewById(R.id.mapaView);
        mapaView.onCreate(savedInstanceState);

        Intent intent = getIntent();
        final String nombre = intent.getStringExtra(Constantes.NOMBRE);
        final double latitud = intent.getDoubleExtra(Constantes.LATITUD, 0);
        final double longitud = intent.getDoubleExtra(Constantes.LONGITUD, 0);
        final String telefono = "Teléfono: " + intent.getIntExtra(Constantes.TELEFONO, 0);

        mapaView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latitud, longitud))
                        .title(nombre)
                        .snippet(telefono));

                CameraPosition position = new CameraPosition.Builder()
                        .target(new LatLng(latitud, longitud)) // Fija la posición
                        .zoom(17) // Fija el nivel de zoom
                        .tilt(30) // Fija la inclinación de la cámara
                        .build();

                mapboxMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(position), 5000);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.menu_nueva_farmacia:
                intent = new Intent(this, NuevaFarmaciaActivity.class);
                intent.putExtra(Constantes.ACCION, Constantes.NUEVA);
                startActivity(intent);
                return true;
            case R.id.menu_listar_farm_fav:
                intent = new Intent(this, FavFarmActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_acerca_de:
                intent = new Intent(this, AcercaDe.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }
}
