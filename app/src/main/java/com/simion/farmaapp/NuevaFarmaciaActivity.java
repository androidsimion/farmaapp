package com.simion.farmaapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.simion.base.BaseDatos;
import com.simion.base.Farmacia;
import com.simion.util.Constantes;
import com.simion.util.Utils;

import java.awt.font.NumericShaper;

public class NuevaFarmaciaActivity extends AppCompatActivity implements View.OnClickListener {

    Farmacia farmacia;
    String accion;
    EditText etNombre;
    EditText etDirrection;
    EditText etTelefono;
    EditText etHorario;
    ImageView ivFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_farmacia);

        Button btAceptar = (Button) findViewById(R.id.btAceptar);
        Button btLimpiar = (Button) findViewById(R.id.btLimpiar);

        btAceptar.setOnClickListener(this);
        btLimpiar.setOnClickListener(this);

        etNombre = (EditText) findViewById(R.id.etNombre);
        etDirrection = (EditText) findViewById(R.id.etDireccion);
        etTelefono = (EditText) findViewById(R.id.etTelefono);
        etHorario = (EditText) findViewById(R.id.etHorario);
        ivFoto = (ImageView) findViewById(R.id.ivFoto);
        ivFoto.setDrawingCacheEnabled(true);
        Intent intent = getIntent();
        accion = intent.getStringExtra(Constantes.ACCION);
        if (accion.equals(Constantes.MODIFICAR)){
            etNombre.setText(intent.getStringExtra(Constantes.NOMBRE));
            etDirrection.setText(intent.getStringExtra(Constantes.DIRECCION));
            etTelefono.setText(String.valueOf(intent.getIntExtra(Constantes.TELEFONO, 0)));
            etHorario.setText(intent.getStringExtra(Constantes.HORARIO));
            ivFoto.setImageBitmap(Utils.getBitmap(intent.getByteArrayExtra(Constantes.IMAGES)));

        }
        else if (accion.equals(Constantes.NUEVA)){
            ivFoto.setImageResource(R.drawable.farmacia);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btAceptar){
            if (accion.equals(Constantes.MODIFICAR)){
                Intent intent = getIntent();
                farmacia = new Farmacia();
                farmacia.setId(intent.getIntExtra(Constantes._ID, 0));
                farmacia.setLatitud(intent.getDoubleExtra(Constantes.LATITUD, 0));
                farmacia.setLongitud(intent.getDoubleExtra(Constantes.LONGITUD, 0));
                try{
                    String texto = etTelefono.getText().toString();
                    farmacia.setTelefono(Integer.parseInt(texto));
                    Toast.makeText(this, "Telefno guardado m", Toast.LENGTH_SHORT);
                }catch (NumberFormatException nfe){
                    String mensaje = "El número de telefo no tiene un formato correcto.";
                    Toast.makeText(this, mensaje, Toast.LENGTH_LONG);
                    farmacia.setTelefono(0);
//                    return;
                }
                farmacia.setNombre(etNombre.getText().toString());
                farmacia.setDireccion(etDirrection.getText().toString());
                farmacia.setHorario(etHorario.getText().toString());
                farmacia.setFoto(ivFoto.getDrawingCache());
                BaseDatos baseDatos = new BaseDatos(this);
                baseDatos.modificarFarmacia(farmacia);
                this.finish();
            }
            else if (accion.equals(Constantes.NUEVA)){
                farmacia = new Farmacia();
                try{
                    farmacia.setTelefono(Integer.parseInt(etTelefono.getText().toString()));
                    System.out.println("Telefno guardado");
                }catch (NumberFormatException nfe){
                    String mensaje = "El número de telefono no tiene un formato correcto.";
                    Toast.makeText(this, mensaje, Toast.LENGTH_LONG);
                    return;
                }
                farmacia.setNombre(etNombre.getText().toString());
                farmacia.setDireccion(etDirrection.getText().toString());
                farmacia.setHorario(etHorario.getText().toString());
                farmacia.setFoto(ivFoto.getDrawingCache());
                farmacia.setLatitud(0);
                farmacia.setLongitud(0);
                BaseDatos baseDatos = new BaseDatos(this);
                baseDatos.nuevaFarmacia(farmacia);
                this.finish();
            }
        }
        else if (v.getId() == R.id.btLimpiar){
        }

    }
}
