package com.simion.farmaapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.simion.base.Farmacia;
import com.simion.util.Constantes;
import com.simion.util.Utils;


import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class OpinarActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etComentario;
    TextView tvNombre;
    EditText etPuntuacion;
    private ImageView ivFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opinar);


        Farmacia farmacia = new Farmacia();
        Intent intent = getIntent();
        String nombre = intent.getStringExtra(Constantes.NOMBRE);
        String direccion = intent.getStringExtra(Constantes.DIRECCION);
        String telefono = "Teléfono: " + intent.getIntExtra(Constantes.TELEFONO, 0);
        Bitmap foto = Utils.getBitmap(intent.getByteArrayExtra(Constantes.IMAGES));

        tvNombre = (TextView) findViewById(R.id.tvNombreOpinar);
        TextView tvDireccion = (TextView) findViewById(R.id.tvDireccionOpinar);
        TextView tvTelefono = (TextView) findViewById(R.id.tvTelefonoOpinar);
        ivFoto = (ImageView) findViewById(R.id.ivFotoOpinar);
        etComentario = (EditText) findViewById(R.id.etComentario);
        etPuntuacion = (EditText) findViewById(R.id.etPuntuacion);

        Button btEnviar = (Button) findViewById(R.id.btEnviarOpinar);
        Button btLimpiar = (Button) findViewById(R.id.btLimpiarOpinar);

        tvNombre.setText(nombre);
        tvDireccion.setText(direccion);
        tvTelefono.setText(telefono);
        ivFoto.setImageBitmap(foto);

        btEnviar.setOnClickListener(this);
        btLimpiar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btEnviarOpinar){
            String nombre = tvNombre.getText().toString();
            String texto = etComentario.getText().toString();
            int puntuacion = Integer.parseInt(etPuntuacion.getText().toString());
            final String sentencia = Constantes.URL_SERVIDOR + "/add_opinion?nombre=" + nombre + "&comentario=" + texto +
                    "&puntuacion=" + puntuacion;
            AsyncTask tarea = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] params) {
                    RestTemplate restTemplate = new RestTemplate();
                    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                    String sentencia = (String) params[0];
                    restTemplate.getForObject(sentencia, Void.class);
                    return null;
                }
            };
            tarea.execute(sentencia);
            this.finish();
        }
        else if (v.getId() == R.id.btLimpiarOpinar){
            etComentario.setText("");
        }
    }
}
