package com.simion.farmaapp;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.simion.base.Opinion;
import com.simion.base.OpinionAdapter;
import com.simion.base.TareaOpinar;
import com.simion.util.Constantes;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;

public class OpinionesActivity extends AppCompatActivity  {

    private ArrayList<Opinion> listaOpiniones;
    private OpinionAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opiniones);

        ListView lvListaOpiniones = (ListView) findViewById(R.id.lvOpinioines);
        listaOpiniones = new ArrayList<>();
        adapter = new OpinionAdapter(this, listaOpiniones);
        lvListaOpiniones.setAdapter(adapter);

        TareaOpinar to = new TareaOpinar(listaOpiniones, adapter);
        to.execute(Constantes.URL_SERVIDOR + "/opiniones");
    }
}
