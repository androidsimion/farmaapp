package com.simion.farmaapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;

import com.simion.base.BaseDatos;
import com.simion.base.Farmacia;
import com.simion.base.FarmaciaAdapter;
import com.simion.base.TareaDescargarDatos;
import com.simion.util.Constantes;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_DISPLAY_LENGTH = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /* New Handler to start the Menu-Activity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, FarmaciasActivity.class));
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

    }


}
