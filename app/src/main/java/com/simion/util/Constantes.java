package com.simion.util;

/**
 * Created by Simion on 07/03/2017.
 */
public class Constantes {

    public static final String URL_JSON = "http://www.zaragoza.es/georref/json/hilo/farmacias_Equipamiento";

    public static final String NOMBRE = "nombre";
    public static final String _ID = "id";
    public static final String DIRECCION = "direccion";
    public static final String TELEFONO = "telefono";
    public static final String HORARIO = "horario";
    public static final String LATITUD = "latitud";
    public static final String LONGITUD = "longitud";
    public static final String IMAGES = "imagen";
    public static final String MODIFICAR = "modificar";
    public static final String ACCION = "accion";
    public static final String NUEVA = "nueva";

    public static final String URL_SERVIDOR = "http://192.168.3.42:8082";;
}
