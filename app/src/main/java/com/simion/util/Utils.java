package com.simion.util;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.simion.farmaapp.AcercaDe;
import com.simion.farmaapp.FarmaciasActivity;
import com.simion.farmaapp.NuevaFarmaciaActivity;
import com.simion.farmaapp.R;

import java.io.ByteArrayOutputStream;

import uk.me.jstott.jcoord.LatLng;
import uk.me.jstott.jcoord.UTMRef;

/**
 * Created by Simion on 07/03/2017.
 */
public class Utils {
    public static LatLng DeUMTSaLatLng(double latitud, double longitud) {

        UTMRef utm = new UTMRef(latitud, longitud, 'N', 30);

        return utm.toLatLng();
    }

    public static byte[] getBytes(Bitmap foto) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        foto.compress(Bitmap.CompressFormat.JPEG, 0, bos);
        return bos.toByteArray();
    }

    public static Bitmap getBitmap(byte[] imagenBytes) {
        return BitmapFactory.decodeByteArray(imagenBytes, 0, imagenBytes.length);

    }
}
